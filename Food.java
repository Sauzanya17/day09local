public class Food
{
    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String inDescription)
    {
      /**
      *Set the description
      *@param inDescription the new Description.
      */
      
      description = inDescription;
    }
    
}